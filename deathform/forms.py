from django import forms
from django.forms import ModelForm
from betterforms.multiform import MultiModelForm
from .models import Deceased
from .models import Claimant
from .models import Relation

class DeceasedForm(ModelForm):
	required_css_class = "required"
	class Meta:
		model = Relation
		fields = '__all__'

class ClaimantForm(ModelForm):
	required_css_class = "required"
	class Meta:
		model = Relation
		fields = '__all__'

class RelationForm(ModelForm):
	required_css_class = "required"
	class Meta:
		model = Relation
		fields = '__all__'

class EntryCreationForm(MultiModelForm):
	form_class = {
		'deceasedf': DeceasedForm,
		'claimantf': ClaimantForm,
		'relationf': RelationForm,
	}