from django.urls import path
from . import views
from .views import RelationListView, RelationDetailView, ClaimantDetailView, DeceasedDetailView, RelationCreateView, DeceasedCreateView, ClaimantCreateView, ClaimantUpdateView, DeceasedUpdateView, RelationUpdateView, RelationDeleteView

urlpatterns = [
    path('', RelationListView.as_view(), name="deathform-home"),
    path('deathform/<int:pk>', RelationDetailView.as_view(), name="deathform-detail"),
    path('deathform/new/', DeceasedCreateView.as_view(), name="deathform-create"),
    path('deathform/new/1', ClaimantCreateView.as_view(), name="deathform-create1"),
    path('deathform/new/2', RelationCreateView.as_view(), name="deathform-create2"),
    path('deathform/<int:pk>/update/', RelationUpdateView.as_view(), name="deathform-update"),
    path('deathform/<int:pk>/delete/', RelationDeleteView.as_view(), name="deathform-delete"),
]