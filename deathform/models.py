from django.db import models
from django.urls import reverse

# create table Claimant(
# c_id numeric(10),
# c_name varchar(50),
# c_birth date,
# c_contact varchar(20),
# primary key	(c_id)
# );

# create table Deceased(
# d_id numeric(10),
# d_name varchar(50),
# d_birth date,
# d_death date,
# primary key	(d_id)
# );

# create table Relation(
# d_id numeric(10),
# c_id numeric(10),
# rel varchar(20),
# primary key (d_id),
# foreign key (d_id) references Deceased(d_id),
# foreign key (c_id) references Claimant(c_id)
# );

# Create your models here.
class Deceased(models.Model):
	d_id = models.CharField('ID', max_length=10, primary_key=True)
	d_name = models.CharField('Name', max_length=50)
	d_birth = models.DateField('Date of Birth')
	d_death = models.DateField('Date of Death')

	def __str__(self):
		return self.d_name

	def get_absolute_url(self):
		return reverse('deathform-create1')
		# return reverse('deathform-update1', kwargs={'pk':self.pk})
		# return reverse('deathform-home')

class Claimant(models.Model):
	c_id = models.CharField('Claimant ID', max_length=10, primary_key=True)
	c_name = models.CharField('Claimant Name', max_length=50)
	c_birth = models.DateField('Claimant Date of Birth')
	c_contact = models.CharField('Claimant Contact Information', max_length=20)

	def __str__(self):
		return self.c_name

	def get_absolute_url(self):
		return reverse('deathform-create2')
		# return reverse('deathform-detail2', kwargs={'pk':self.pk})
		# return reverse('deathform-home')

class Relation(models.Model):
	PROCESSING = 'Processing'
	PROCESSED = 'Processed'
	STATUS_CHOICES = [
		(PROCESSING, 'Processing'),
		(PROCESSED, 'Processed'),
	]
	deceased = models.OneToOneField(Deceased, on_delete=models.CASCADE)
	claimant = models.ForeignKey(Claimant, on_delete=models.CASCADE)
	rel = models.CharField('Status', max_length=10, choices=STATUS_CHOICES, default=PROCESSING)

	def __str__(self):
		return self.deceased.d_name

	def get_absolute_url(self):
		return reverse('deathform-detail', kwargs={'pk':self.pk})