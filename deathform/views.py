from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Deceased
from .models import Claimant
from .models import Relation
from django.shortcuts import redirect

# Create your views here.

context = {
	'relations': Relation.objects.all()
}

def home(request):
	return render(request, "deathform/home.html", context)

# def register(request):


class RelationListView(ListView):
	model = Relation
	template_name = "deathform/home.html" #<app>/<model>_<viewtype>.html
	context_object_name = 'relations'

class RelationDetailView(DetailView):
	model = Relation

class ClaimantDetailView(DetailView):
	model = Claimant

class DeceasedDetailView(DetailView):
	model = Deceased

class ClaimantCreateView(CreateView):
	model = Claimant
	fields = ['c_id', 'c_name', 'c_birth', 'c_contact']



class DeceasedCreateView(CreateView):
	model = Deceased
	fields = ['d_id', 'd_name', 'd_birth', 'd_death']
	
class RelationCreateView(CreateView):
	model = Relation
	fields = ['deceased', 'claimant', 'rel']

class ClaimantUpdateView(UpdateView):
	model = Claimant
	fields = ['c_id', 'c_name', 'c_birth', 'c_contact']



class DeceasedUpdateView(UpdateView):
	model = Deceased
	fields = ['d_id', 'd_name', 'd_birth', 'd_death']


class RelationUpdateView(UpdateView):
	model = Relation
	fields = ['deceased', 'claimant', 'rel']

class RelationDeleteView(DeleteView):
	model = Relation
	success_url = '/'