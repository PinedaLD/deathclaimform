# Generated by Django 2.2.7 on 2019-12-01 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deathform', '0004_auto_20191201_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claimant',
            name='c_id',
            field=models.CharField(max_length=10, primary_key=True, serialize=False, verbose_name='Claimant ID'),
        ),
        migrations.AlterField(
            model_name='deceased',
            name='d_id',
            field=models.CharField(max_length=10, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
