Django==2.2.7
gunicorn==20.0.4
mysqlclient==1.4.4
pytz==2019.3
sqlparse==0.3.0
